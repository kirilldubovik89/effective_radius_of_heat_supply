import scripts.abonents_preprocessing as abonents
import scripts.cadastre_preprocessing as cadastre
import scripts.heat_losses_preprocessing as heat_losses
import scripts.heat_net_preprocessing as heat_net
import scripts.perspective_preprocessing as perspective
import scripts.price_preprocessing as price
import scripts.radius_determination as radius

PATH_TO_DATA = 'data/raw'
PATH_TO_PREPROCESSED = 'data/preprocessed'
PATH_TO_RESULT = 'data/result'


if __name__ == '__main__':
    abonents.get_abonents_preprocessing(PATH_TO_DATA, PATH_TO_PREPROCESSED)
    cadastre.get_cadastre_preprocessing(PATH_TO_DATA, PATH_TO_PREPROCESSED)
    heat_losses.get_heat_losses_preprocessing(PATH_TO_DATA, PATH_TO_PREPROCESSED)
    heat_net.get_heat_net_preprocessing(PATH_TO_DATA, PATH_TO_PREPROCESSED)
    perspective.get_perspective_preprocessing(PATH_TO_DATA, PATH_TO_PREPROCESSED)
    price.get_price_preprocessing(PATH_TO_DATA, PATH_TO_PREPROCESSED)

    radius.get_radius_determination(PATH_TO_PREPROCESSED, PATH_TO_RESULT)
