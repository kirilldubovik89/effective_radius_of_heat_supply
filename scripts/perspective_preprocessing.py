"""
preprocessing of tabular data on perspective
"""

import pandas as pd  # pylint: disable=E0401
import click  # pylint: disable=E0401


@click.argument('path_to_data', type=click.Path(exists=True))
@click.argument('path_to_preprocessed', type=click.Path())
def get_perspective_preprocessing(path_to_data: str, path_to_preprocessed: str):
    """
    preprocessing of tabular data on perspective
    """
    # Перспективные площадки строительства
    perspective_area = pd.read_excel(f'{path_to_data}/perspective_area.xls', header=0)
    perspective_abonents = pd.read_excel(f'{path_to_data}/perspective_abonents.xlsx',
                                         header=0)

    perspective_area = perspective_area[["Sys", "Площадь кадастра _м2"]]
    perspective_area = perspective_area\
        .rename(columns={"Sys": "ID", "Площадь кадастра _м2": "S"})

    perspective_abonents = perspective_abonents.groupby(['ID'])\
        .agg({'Нагрузка на отопление, Гкал/ч': 'sum',
              'Нагрузка на вентиляцию Гкал/ч': 'sum',
              'Нагрузка на ГВС средняя, Гкал/ч': 'sum',
              'Сумма при среднем ГВС, Гкал/ч': 'sum',
              'Годовое потребление тепловой энергии, Гкал': 'sum'})\
        .reset_index()

    perspective_abonents['Qhv'] = \
        (perspective_abonents['Нагрузка на отопление, Гкал/ч'] +
         perspective_abonents['Нагрузка на вентиляцию Гкал/ч'])

    perspective_abonents\
        .rename(columns={'Нагрузка на ГВС средняя, Гкал/ч': 'Qhws',
                         'Сумма при среднем ГВС, Гкал/ч': 'Qsum',
                         'Годовое потребление тепловой энергии, Гкал': 'Qyear'},
                inplace=True)

    perspective_abonents = perspective_abonents[['ID', 'Qhv', 'Qhws', 'Qsum', 'Qyear']]

    perspective = perspective_area.merge(perspective_abonents, on=['ID'], how='left')

    perspective['G'] = perspective['Qhv']*12.5 + perspective['Qhws']*21.9

    perspective.to_csv(f'{path_to_preprocessed}/perspective_preprocessed.csv')
    print('Pre-processing of perspective data is complete')
