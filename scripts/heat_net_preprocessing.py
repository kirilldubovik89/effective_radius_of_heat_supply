"""
preprocessing of tabular data on heating networks
"""

import pandas as pd  # pylint: disable=E0401
import click  # pylint: disable=E0401
from classes.diametres import dint_to_dn  # pylint: disable=E0401


@click.argument('path_to_data', type=click.Path(exists=True))
@click.argument('path_to_preprocessed', type=click.Path())
def get_heat_net_preprocessing(path_to_data: str, path_to_preprocessed: str):
    """
    preprocessing of tabular data on heating networks
    """
    heat_net = pd.read_excel(f'{path_to_data}/heating_networks.xls',
                             index_col=0, header=0)
    heat_net.index.name = None

    # Удаляем участки тепловых сетей, проложенных по подвалу, а также
    # участки тепловых сетей за ЦТП, имеющих пониженные параметры теплоносителя
    heat_net = heat_net[(heat_net['Вид прокладки тепловой сети'] != 'Подвальная') &
                        (heat_net['Температура в начале участка под.тр-да,°C'] >= 130)]

    # Удаляем лишние колонки
    useless_heat_net = ['Номер источника',
                        'Наименование начала участка',
                        'Наименование конца участка',
                        'Внутренний диаметр обратного трубопровода, м',
                        'Вид прокладки тепловой сети',
                        'Нормативные потери в тепловой сети (1-4)',
                        'Расход воды в подающем трубопроводе, т/ч',
                        'Расход воды в обратном трубопроводе, т/ч',
                        'Потери напора в подающем трубопроводе, м',
                        'Потери напора в обратном трубопроводе, м',
                        'Удельные линейные потери напора в под.тр-де, мм/м',
                        'Удельные линейные потери напора в обр.тр-де, мм/м',
                        'Скорость движения воды в под.тр-де, м/с',
                        'Скорость движения воды в обр.тр-де, м/с',
                        'Температура в начале участка под.тр-да,°C',
                        'Температура в конце участка под.тр-да,°C',
                        'Температура в начале участка обр.тр-да,°C',
                        'Температура в конце участка обр.тр-да,°C']

    heat_net = heat_net.drop(columns=useless_heat_net)

    # Добавляем в базу участков условные диаметры тепловых сетей
    heat_net['DN'] = heat_net['Внутpенний диаметp подающего тpубопpовода, м']
    heat_net['DN'].replace(dint_to_dn, inplace=True)

    heat_net.rename(columns={'Кадастр': 'cadastre',
                             'Длина участка, м': 'length',
                             'Внутpенний диаметp подающего тpубопpовода, м': 'Dint'},
                    inplace=True)

    heat_net.to_csv(f'{path_to_preprocessed}/heat_net_preprocessed.csv')
    print('Pre-processing of heat network data is complete')
