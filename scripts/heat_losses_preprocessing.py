"""
preprocessing of tabular data on heat losses
"""

import pandas as pd  # pylint: disable=E0401
import numpy as np  # pylint: disable=E0401
import click  # pylint: disable=E0401
from classes.diametres import dn_to_dint  # pylint: disable=E0401

# среднегодовая температура теплоносителя в тепловых сетях
T_HC_MEAN = 68.2

# среднегодовая температура грунта
T_GR_MEAN = 5.8

# среднегодовая температура холодной воды на входе в систему теплоснабжения
T_CW_MEAN = (5328 * 5 + 3072 * 15) / 8400


@click.argument('path_to_data', type=click.Path(exists=True))
@click.argument('path_to_preprocessed', type=click.Path())
def get_heat_losses_preprocessing(path_to_data: str, path_to_preprocessed: str):
    """
    preprocessing of tabular data on heat losses
    """
    heat_losses = pd.read_excel(f'{path_to_data}/norms_of_heat_losses.xls',
                                header=0)

    heat_losses = heat_losses\
        .rename(columns={'Разность температур теплоносителя и грунта, °С': 'dt'})
    heat_losses = heat_losses[heat_losses['dt'] == round(T_HC_MEAN-T_GR_MEAN, 1)]
    heat_losses = heat_losses.drop(columns=['dt']).transpose().reset_index()
    heat_losses.columns = ['DN', 'dQ_is']
    heat_losses = heat_losses[heat_losses['DN'] != 25]
    heat_losses['d_int'] = heat_losses['DN']
    heat_losses['d_int'].replace(dn_to_dint, inplace=True)
    heat_losses['V'] = (2*np.pi*heat_losses['d_int']**2)/4  # объем
    heat_losses = heat_losses[['DN', 'd_int', 'V', 'dQ_is']]
    heat_losses['dQ_leak'] = heat_losses['V']*0.0025*(T_HC_MEAN-T_CW_MEAN)*1000
    heat_losses['dQ'] = heat_losses['dQ_is']+heat_losses['dQ_leak']

    heat_losses.to_csv(f'{path_to_preprocessed}/heat_losses_preprocessed.csv')
    print('Pre-processing of heat_losses data is complete')
