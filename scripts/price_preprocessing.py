"""
preprocessing of tabular data on construction price of heating networks
"""

import pandas as pd  # pylint: disable=E0401
from sklearn.preprocessing import PolynomialFeatures  # pylint: disable=E0401
from sklearn.linear_model import LinearRegression  # pylint: disable=E0401
import click  # pylint: disable=E0401

# Рассчитаем итоговую удельную стоимость строительства тепловой сети.
# Для этого определим все необходимые коэффициенты.

# Индексы-дефляторы
I_2020 = 1.0491
I_2021 = 1.0839

# НДС
K_VAT = 1.2

# Коэффициент перехода от цен Московской обл. к уровню цен Новосибирской обл.
K_CONV = 0.93

# Коэффициент, учитывающий изменение стоимости строительства
# на территории Новосибирской обл., связанный с климатическими условиями
K_CLIMATE = 1.02

# Коэффициент, учитывающий стесненные условия строительства
K_TIGHT = 1.06


@click.argument('path_to_data', type=click.Path(exists=True))
@click.argument('path_to_preprocessed', type=click.Path())
def get_price_preprocessing(path_to_data: str, path_to_preprocessed: str):
    """
    preprocessing of tabular data on construction price of heating networks
    """
    price = pd.read_excel(f'{path_to_data}/construction_cost_standards.xls',
                          index_col=0, header=0)
    price.index.name = None

    price = price[['DN', 'Норматив цены строительства на 01.01.2020, тыс.руб./м']]
    price = price.rename(
        columns={"Норматив цены строительства на 01.01.2020, тыс.руб./м": "cost_2020"})

    poly = PolynomialFeatures(degree=2, include_bias=False)

    x_train = price[price['cost_2020'].notna()]['DN'].values
    y_train = price[price['cost_2020'].notna()]['cost_2020']

    poly_features_train = poly.fit_transform(x_train.reshape(-1, 1))
    poly_reg_model = LinearRegression()
    poly_reg_model.fit(poly_features_train, y_train)

    x_values = price['DN'].values
    poly_features = poly.fit_transform(x_values.reshape(-1, 1))
    y_predicted = poly_reg_model.predict(poly_features)

    # Заполним отсутствующие значения стоимостей прокладки тепловых сетей
    price.loc[price['DN'] == 32, ['cost_2020']] = y_predicted[0]
    price.loc[price['DN'] == 40, ['cost_2020']] = y_predicted[1]

    price['cost_sp'] = price['cost_2020']*I_2020*I_2021*K_CONV*K_CLIMATE*K_TIGHT*K_VAT

    price.to_csv(f'{path_to_preprocessed}/price_preprocessed.csv')
    print('Pre-processing of price data is complete')
