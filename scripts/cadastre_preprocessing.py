"""
preprocessing of tabular data on cadastre
"""

import pandas as pd  # pylint: disable=E0401
import click  # pylint: disable=E0401


@click.argument('path_to_data', type=click.Path(exists=True))
@click.argument('path_to_preprocessed', type=click.Path())
def get_cadastre_preprocessing(path_to_data: str, path_to_preprocessed: str):
    """
    preprocessing of tabular data on cadastre
    """
    cadastre = pd.read_excel(f'{path_to_data}/cadastre.xls', index_col=0, header=0)
    cadastre.index.name = None

    cadastre = cadastre[["КН", "Площадь"]]
    cadastre = cadastre.rename(columns={"КН": "cadastre", "Площадь": "S"})

    cadastre.to_csv(f'{path_to_preprocessed}/cadastre_preprocessed.csv')
    print('Pre-processing of cadastre data is complete')
