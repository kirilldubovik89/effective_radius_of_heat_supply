"""
Determination of the effective radius of heat supply
"""

import pickle  # pylint: disable=E0401
import mlflow  # pylint: disable=E0401
import pandas as pd  # pylint: disable=E0401
import numpy as np  # pylint: disable=E0401
from sklearn.linear_model import LinearRegression  # pylint: disable=E0401
from sklearn.metrics import mean_squared_error  # pylint: disable=E0401
import click  # pylint: disable=E0401
from classes.dn_branch import get_dn_branch  # pylint: disable=E0401

T_ABONENTS = 1505.98
T_SOURCE = 568.62

# Зададимся значением простого срока окупаемости, в зависимости от которого будет
# рассчитан эффективный радиус теплоснабжения
PP = 5


@click.argument('path_to_preprocessed', type=click.Path(exists=True))
def get_df_preprocessed(path_to_preprocessed: str):
    """
    Get preprocessed data
    """
    heat_losses_pr = pd.read_csv(f'{path_to_preprocessed}/heat_losses_preprocessed.csv')
    heat_net_pr = pd.read_csv(f'{path_to_preprocessed}/heat_net_preprocessed.csv')
    price_pr = pd.read_csv(f'{path_to_preprocessed}/price_preprocessed.csv')
    df_pr = heat_net_pr.merge(price_pr, on=['DN'], how='left')

    df_pr['total_cost'] = df_pr['cost_sp'] * df_pr['length']

    # Добавим в базу участков потери тепловой энергии
    # в зависимости от условного диаметра
    df_pr = df_pr.merge(heat_losses_pr[['DN', 'dQ']], on=['DN'], how='left')
    df_pr['dQ_year'] = df_pr['dQ']*df_pr['length']*8400/10**6
    return df_pr


mlflow.set_experiment('LinearRegression')


@click.argument('path_to_preprocessed', type=click.Path(exists=True))
@click.argument('path_to_result', type=click.Path())
def get_radius_determination(path_to_preprocessed: str, path_to_result: str):
    """
    Determination of the effective radius of heat supply
    """
    with mlflow.start_run():
        abonents_pr = pd.read_csv(f'{path_to_preprocessed}/abonents_preprocessed.csv')
        cadastre_pr = pd.read_csv(f'{path_to_preprocessed}/cadastre_preprocessed.csv')
        perspective_pr = pd.read_csv(f'{path_to_preprocessed}/'
                                     f'perspective_preprocessed.csv')
        df_pr = get_df_preprocessed(path_to_preprocessed)
        heat_losses_pr = pd.read_csv(f'{path_to_preprocessed}/'
                                     f'heat_losses_preprocessed.csv')
        price_pr = pd.read_csv(f'{path_to_preprocessed}/price_preprocessed.csv')

        df_fit = df_pr.groupby(['cadastre']).agg({'total_cost': 'sum',
                                                  'dQ_year': 'sum'})
        df_fit = df_fit.reset_index()\
                       .merge(cadastre_pr[['cadastre', 'S']],
                              on=['cadastre'], how='left')\
                       .merge(abonents_pr, on=['cadastre'], how='left')

        df_fit = df_fit[['cadastre', 'S', 'Qsum', 'total_cost', 'dQ_year']].dropna()

        df_fit = df_fit[(df_fit['S'] < df_fit.S.quantile(0.95)) &
                        (df_fit['Qsum'] < df_fit.Qsum.quantile(0.95))]

        x_lin = df_fit[["S", "Qsum"]]
        y_cost = df_fit['total_cost']
        y_dq = df_fit['dQ_year']

        model_lin_cost = LinearRegression()
        model_lin_cost.fit(x_lin, y_cost)

        with open('models/model_lin_cost.pkl', mode='wb') as file:
            pickle.dump(model_lin_cost, file)

        model_lin_dq = LinearRegression()
        model_lin_dq.fit(x_lin, y_dq)

        with open('models/model_lin_dq.pkl', mode='wb') as file:
            pickle.dump(model_lin_dq, file)

        x_predict = perspective_pr[["S", "Qsum"]]

        # Ожидаемая стоимость строительства внутриквартальных тепловых сетей
        perspective_pr['cost_predict'] = model_lin_cost.predict(x_predict)

        # Ожидаемые годовые потери во внутриквартальных тепловых сетях
        perspective_pr['dQ_year_predict'] = model_lin_dq.predict(x_predict)

        # Диаметр нового ответвления для подключения перспективной застройки
        perspective_pr['DN_branch'] = perspective_pr.apply(lambda x: get_dn_branch(x.G),
                                                           axis=1)

        # Количество трубопроводов
        perspective_pr['n_pipes'] = np.where(perspective_pr['DN_branch'] >= 300, 3, 2)

        # Коэффициент, учитывающий прокладку трубопроводов в три нитки
        perspective_pr['K_res'] = np.where(perspective_pr['n_pipes'] == 3, 1.39, 1)

        # Удельные тепловые потери нового ответвления
        perspective_pr = pd.merge(perspective_pr,
                                  heat_losses_pr[['DN', 'dQ']],
                                  left_on='DN_branch',
                                  right_on='DN')

        # Удельная стоимость строительства нового ответвления
        perspective_pr = pd.merge(perspective_pr,
                                  price_pr[['DN', 'cost_sp']],
                                  on='DN')

        perspective_pr = perspective_pr.drop(columns=['DN'])

        # Эффективный радиус теплоснабжения
        perspective_pr['r_ef'] = (perspective_pr['Qyear']*T_ABONENTS*PP -
                                  perspective_pr['Qyear']*T_SOURCE*PP -
                                  perspective_pr['dQ_year_predict']*T_SOURCE*PP -
                                  perspective_pr['cost_predict']*1000) /\
                                 (perspective_pr['cost_sp'] *
                                  perspective_pr['K_res']*1000 +
                                  8400*perspective_pr['dQ']*1.15 *
                                  perspective_pr['n_pipes'] /
                                  2*T_SOURCE*PP/10**6)

        perspective_pr.to_csv(f'{path_to_result}/effictive_radius_determination.csv')
        print('Radius determination is complete')

    params = {'payback period': PP,
              'tariff for heat energy in hot water': T_ABONENTS,
              'tariff for heat energy on the collectors of heat sources ': T_SOURCE}

    x_lin = df_fit[["S", "Qsum"]]

    y_cost = df_fit['total_cost']
    y_dq = df_fit['dQ_year']

    model_lin_cost_score = LinearRegression()
    model_lin_cost_score.fit(x_lin, y_cost)

    model_lin_dq_score = LinearRegression()
    model_lin_dq_score.fit(x_lin, y_dq)

    y_lin_cost_pred = model_lin_cost_score.predict(x_lin)
    y_lin_dq_pred = model_lin_dq_score.predict(x_lin)

    rmse_cost = [np.sqrt(mean_squared_error(y_cost, y_lin_cost_pred))]
    diff_total_cost = df_fit['total_cost'].max() - df_fit['total_cost'].min()
    rmse_cost_n = [rmse_cost[0] / diff_total_cost]

    rmse_dq = [np.sqrt(mean_squared_error(y_dq, y_lin_dq_pred))]
    diff_dq = df_fit['dQ_year'].max() - df_fit['dQ_year'].min()
    rmse_dq_n = [rmse_dq[0] / diff_dq]

    score = {'RMSE (cost)': rmse_cost_n,
             'RMSE (dQ)': rmse_dq_n}

    mlflow.log_params(params)
    mlflow.log_metrics(score)
    mlflow.log_artifact('models/model_lin_cost.pkl')
    mlflow.log_artifact('models/model_lin_dq.pkl')
