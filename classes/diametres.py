"""
Перевод внутренних диаметров трубопроводов тепловых сетей в условные
"""
dint_to_dn = {0.021: 32,
              0.027: 32,
              0.033: 32,
              0.04: 40,
              0.05: 50,
              0.069: 65,
              0.082: 80,
              0.1: 100,
              0.125: 125,
              0.150: 150,
              0.203: 200,
              0.207: 200,
              0.259: 250,
              0.309: 300,
              0.357: 350,
              0.359: 350,
              0.408: 400,
              0.466: 450,
              0.515: 500,
              0.614: 600,
              0.702: 700,
              0.804: 800,
              0.902: 900,
              1: 1000,
              1.192: 1200,
              1.392: 1400}

# Перевод диаметров в обратную сторону, т.е. из условных во внутренние
dn_to_dint = {v: k for k, v in dint_to_dn.items()}
