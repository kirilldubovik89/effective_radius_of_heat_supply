"""
Определение диаметра нового ответвления в зависимости от расхода сетевой воды
"""


def get_dn_branch(consumption):
    """Определение диаметра нового ответвления в зависимости от расхода сетевой воды"""
    pipeline_size = [40, 50, 65, 80, 100, 125, 150, 200, 250, 300, 350, 400]
    if consumption <= 1.7:
        n_size = 0
    elif consumption <= 3:
        n_size = 1
    elif consumption <= 6.5:
        n_size = 2
    elif consumption <= 11:
        n_size = 3
    elif consumption <= 18:
        n_size = 4
    elif consumption <= 32:
        n_size = 5
    elif consumption <= 58:
        n_size = 6
    elif consumption <= 125:
        n_size = 7
    elif consumption <= 215:
        n_size = 8
    elif consumption <= 350:
        n_size = 9
    elif consumption <= 520:
        n_size = 10
    else:
        n_size = 11
    return pipeline_size[n_size]
